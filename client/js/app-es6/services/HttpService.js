export class HttpService {
    // res.ok

    _handleResp(resp){
        if(!resp.ok) throw new Error(resp.statusText)
        return resp;
    }

    get(url) {
        return fetch(url)
            .then(res => this._handleResp(res))
            .then(resp => resp.json())
              
    }

    post(url, dado){
        return fetch(url, {
            headers:{
                'Content-type': "application/json",
            },
            method: 'post',
            body: JSON.stringify(dado),
        }).then(res => this._handleResp(res));
    }
}