
//Metodo antigo usando Module Pattern, vide @ConnectionFactory_naoModularizado.js
//Como agora estamos usando modulos, não precisamos mais no Module Pattern
//as variaveis não são acessíveis, uma vez que esta modularizado a classe

const stores = ['negociacoes'];
const version = 4;
const dbName = 'aluraframe';

let connection = null;

let close;
/*
    Fábrica de conexão para o indexedDB do browser
    Utilizada para guardar tabelas que acomodam objetos
*/
export class ConnectionFactory{
        
    constructor(){
        throw new Error('Não é possivel criar instâncias de ConnectionFactory');
    }
    static getConnection(){
        
        return new Promise((resolve, reject) => {
            let openRequest = window.indexedDB.open(dbName,version);
            
            openRequest.onupgradeneeded = e => {   
                //e.target.result é uma instancia de IDBDatabase       
                ConnectionFactory._createStores(e.target.result);
            }
            
            openRequest.onsuccess = e => {
                if(!connection){
                    connection = e.target.result;
                    //Monkey Patch , adicionar uma função nova em uma ja existente
                    //Forçar modificação de uma API
                    //guardar metodo close antes de usar o Monkey Patch
                    close = connection.close.bind(connection); //linkar com o object connection
                    connection.close = function(){
                        throw new Error("Você não pode fechar diretamente a conexão");
                    };
                }                     
                resolve(connection);
            }

            openRequest.onerror = e => {
                reject(e.target.error.name)
            }
        });
    }

    static _createStores(connection){
        stores.forEach(store => {
            if (connection.objectStoreNames.contains(store)) connection.deleteObjectStore(store);
            connection.createObjectStore(store,{ autoIncrement: true});
        });
    }

    static closeConnection(){
        if(!connection) {
            close();
            connection = null;
        }      
    } 
};
