'use strict';

System.register([], function (_export, _context) {
    "use strict";

    var _createClass, ConnectionFactory;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    return {
        setters: [],
        execute: function () {
            _createClass = function () {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || false;
                        descriptor.configurable = true;
                        if ("value" in descriptor) descriptor.writable = true;
                        Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }

                return function (Constructor, protoProps, staticProps) {
                    if (protoProps) defineProperties(Constructor.prototype, protoProps);
                    if (staticProps) defineProperties(Constructor, staticProps);
                    return Constructor;
                };
            }();

            ConnectionFactory = function () {
                var stores = ['negociacoes'];
                var version = 4;
                var dbName = 'aluraframe';

                var connection = null;

                var close;
                /*
                    Fábrica de conexão para o indexedDB do browser
                    Utilizada para guardar tabelas que acomodam objetos
                */
                return function () {
                    function ConnectionFactory() {
                        _classCallCheck(this, ConnectionFactory);

                        throw new Error('Não é possivel criar instâncias de ConnectionFactory');
                    }

                    _createClass(ConnectionFactory, null, [{
                        key: 'getConnection',
                        value: function getConnection() {

                            return new Promise(function (resolve, reject) {
                                var openRequest = window.indexedDB.open(dbName, version);

                                openRequest.onupgradeneeded = function (e) {
                                    //e.target.result é uma instancia de IDBDatabase       
                                    ConnectionFactory._createStores(e.target.result);
                                };

                                openRequest.onsuccess = function (e) {
                                    if (!connection) {
                                        connection = e.target.result;
                                        //Monkey Patch , adicionar uma função nova em uma ja existente
                                        //Forçar modificação de uma API
                                        //guardar metodo close antes de usar o Monkey Patch
                                        close = connection.close.bind(connection); //linkar com o object connection
                                        connection.close = function () {
                                            throw new Error("Você não pode fechar diretamente a conexão");
                                        };
                                    }
                                    resolve(connection);
                                };

                                openRequest.onerror = function (e) {
                                    reject(e.target.error.name);
                                };
                            });
                        }
                    }, {
                        key: '_createStores',
                        value: function _createStores(connection) {
                            stores.forEach(function (store) {
                                if (connection.objectStoreNames.contains(store)) connection.deleteObjectStore(store);
                                connection.createObjectStore(store, { autoIncrement: true });
                            });
                        }
                    }, {
                        key: 'closeConnection',
                        value: function closeConnection() {
                            if (!connection) {
                                close();
                                connection = null;
                            }
                        }
                    }]);

                    return ConnectionFactory;
                }();
            }();
        }
    };
});
//# sourceMappingURL=ConnectionFactory_naoModularizado.js.map